class Comment < ActiveRecord::Base
  belongs_to :post
  # put some validation stuff here
  validates_presence_of :post_id
  validates_presence_of :body
end
